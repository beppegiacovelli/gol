package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class GridTest {

	@Test
	public void testConstants() {
		assertEquals("-", Grid.DEAD);
		assertEquals("*", Grid.ALIVE);
	}
	
	@Test
	public void shouldReturnWidth() throws Exception {
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		Grid grid = new Grid(cells, 3, 3);
		
		assertEquals(3, grid.getWidth());
	}
	
	@Test(expected = CustomLifeException.class)
	public void shoulNotCreateGrid() throws Exception {
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		Grid grid = new Grid(cells, 0, 3);

	}
	
	@Test
	public void shouldReturnHeight() throws Exception {
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		Grid grid = new Grid(cells, 3, 3);
		
		assertEquals(3, grid.getHeight());
	}
	
	@Test
	public void shouldTickRight() throws Exception {
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,true);
		
		Grid grid = new Grid(cells, 3, 3);
		
		grid = grid.tick();
		
		assertEquals("---\n-**\n---", grid.getLife());

	}

	@Test
	public void shouldReturn3() throws Exception {
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,true);
		
		Grid grid = new Grid(cells, 3, 3);

		assertEquals(3, grid.numberOfAliveCells());

	}
	
	
	@Test
	public void shouldReturn6() throws Exception {
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,true);
		
		Grid grid = new Grid(cells, 3, 3);

		assertEquals(6, grid.numberOfDeadCells());

	}
}
