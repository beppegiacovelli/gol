package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class CellTest {

	@Test
	public void cellShouldBeAlive() throws Exception {

		Cell cell = new Cell(0, 0, true);
		
		assertTrue(cell.isAlive());
	}

	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseException() throws Exception {

		Cell cell = new Cell(0, -1, true);
		
	}
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionToo() throws Exception {

		Cell cell = new Cell(-1, 0, true);
		
	}
	
	@Test
	public void cellShouldChangeToDead() throws Exception {

		Cell cell = new Cell(0, 0, true);
		
		cell.setAlive(false);
		
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void shouldReurnX() throws Exception {

		Cell cell = new Cell(1, 0, true);
		
		assertEquals(1, cell.getX());
	}

	@Test
	public void shouldReurnY() throws Exception {

		Cell cell = new Cell(1, 0, true);
		
		assertEquals(0, cell.getY());
	}
	
	@Test
	public void cellShouldHaveAliveNeighbors() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertEquals(3, cells[1][1].getNumberOfAliveNeighbors());
	}

	@Test
	public void cellAliveShouldSurvive() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willSurvive());
	}
	
	@Test
	public void cellAliveShouldSurviveToo() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willSurvive());
	}
	
	@Test
	public void cellAliveShouldNotSurvive() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[0][2].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[0][2].willSurvive());
	}
	
	@Test
	public void cellDeadShouldNotSurvive() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellShouldDie() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willDie());
	}
	
	@Test
	public void aliveCellShouldDieToo() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willDie());
	}
	
	@Test
	public void aliveCellShouldNotDie() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void deadCellCantDie() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void deadCellWillRevive() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertTrue(cells[1][1].willRevive());
	}
	
	@Test
	public void deadCellWillNotRevive() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,true);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willRevive());
	}
	
	@Test
	public void aliveCellCantRevive() throws Exception{
		
		Cell[][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0,0,true);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		assertFalse(cells[1][1].willRevive());
	}
	
	
}
